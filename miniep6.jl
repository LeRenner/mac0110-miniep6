# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    num = n^3
    i = 1

    while i <= ( num ÷ 2 + num % 2 )
        sum = 0
        j = i
        while j <= num
            sum += j
            if sum == num
                otherSum = 0
                k = 0
                while otherSum != num
                    otherSum += i
                    otherSum += k
                    k += 2
                end
                if k ÷ 2 == n
                    return i
                end
            end
            j += 2
        end
        i += 2
    end
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    num = m^3
    i = 1

    while i <= ( num ÷ 2 + num % 2 )
        sum = 0
        j = i
        while j <= num
            sum += j
            if sum == num
                otherSum = 0
                k = 0
                while otherSum != num
                    otherSum += i
                    otherSum += k
                    k += 2
                end
                if k ÷ 2 == m
                    print(m, " ", num, " ")
                    maiorNumero = k + i
                    while i < maiorNumero
                        print(i, " ")
                        i += 2
                    end
                    println()
                end
            end
            j += 2
        end
        i += 2
    end
end

function mostra_n(n)
    i = 1
    while i <= n
        imprime_impares_consecutivos(i)
        i += 1
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
